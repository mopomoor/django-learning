.PHONY: all

ENV_DIR = $(CURDIR)/env
SHELL = /bin/bash

all: run

init:
	( \
		sudo pip install virtualenv; \
		virtualenv env; \
		. $(ENV_DIR)/bin/activate; \
		pip install -r requirements.txt; \
		deactivate; \
	)

runserver:
	( \
		. $(ENV_DIR)/bin/activate; \
		cd learning && python manage.py runserver; \
		deactivate; \
	)

test:
	( \
		. $(ENV_DIR)/bin/activate; \
		cd learning && python manage.py test; \
		deactivate; \
	)


