# Django Learning

## install requirements
`make init`

## run server
`make runserver`

## run test
`make test`

## requests

* create a new user: `curl -d "username=mahshadm&password=Mah@137&email=mopomoor@gmail.com" http://127.0.0.1:8000/users/`
* user login: `curl -d "username=mahshadm&password=Mah@137" http://127.0.0.1:8000/login/` - This method return a Token if user authentication is successful

* get posts: `curl -H "Authorization: Token <token>" http://127.0.0.1:8000/posts/`
* get a post: `curl -H "Authorization: Token <token>" http://127.0.0.1:8000/posts/2/`
* create a new post: `curl -H "Authorization: Token <token>" -d "title=Hi&content=Salam&created_by=1" http://127.0.0.1:8000/posts/`

[Note] Make sure you replace `<token>` with the Token number