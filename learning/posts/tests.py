from django.contrib.auth import get_user_model

from rest_framework.test import APITestCase, APIClient

from . import views

# Create your tests here.


class TestPost(APITestCase):
    def setUp(self):
        self.user = self.setup_user()
        self.client = APIClient()
        self.uri = '/posts/'

    @staticmethod
    def setup_user():
        User = get_user_model()

        return User.objects.create_user(
            'test',
            email='test@test.com',
            password='te@1234st'
        )

    def test_posts_list(self):
        self.client.login(username="test", password="te@1234st")
        response = self.client.get(self.uri)

        self.assertEqual(response.status_code, 200,
                         'Expected Response Code 200, received {0} instead.'.format(response.status_code))

    def test_post_create(self):
        self.client.login(username="test", password="te@1234st")

        params = {
            "title": "Say Hello To...",
            "content": "Hello World!",
            "created_by": 1
        }

        response = self.client.post(self.uri, params)

        self.assertEqual(response.status_code, 201,
                         'Expected Response Code 201, received {0} instead.'.format(response.status_code))
