from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter()
router.register('posts', views.PostViewSet, base_name='posts')

app_name = 'posts'
urlpatterns = [
    path('users/', views.UserCreate.as_view(), name='users_create'),
    path("login/", obtain_auth_token, name="login"),
]

urlpatterns += router.urls
