from rest_framework import generics, views, viewsets
from django.contrib.auth import authenticate
from django.shortcuts import get_object_or_404

from . import models
from .serializers.post import PostSerializer
from .serializers.user import UserSerializer


class PostViewSet(viewsets.ModelViewSet):
    queryset = models.Post.objects.all()
    serializer_class = PostSerializer


class UserCreate(generics.CreateAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = UserSerializer
